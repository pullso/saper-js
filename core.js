function getMatrix(cols, rows) {
  const matrix = []
  let idCounter = 1

  for (let y = 0; y < rows; y++) {
    const row = []
    for (let x = 0; x < cols; x++) {
      row.push({
        id: idCounter++,
        left: false,
        right: false,
        flag: false,
        mine: false,
        show: false,
        poten: false,
        minesAround: 0,
        x,
        y,
      })

    }
    matrix.push(row)
  }

  return matrix
}

function getRandomFreeCell(matrix) {
  const freeCells = []
  for (let y = 0; y < matrix.length; y++) {
    for (let x = 0; x < matrix[y].length; x++) {
      const cell = matrix[y][x]
      if (!cell.mine) freeCells.push(cell)
    }
  }
  const idx = Math.floor(Math.random() * freeCells.length)
  return freeCells[idx]
}

function setRandomMine(matrix) {
  const cell = getRandomFreeCell(matrix)
  cell.mine = true

  const cells = getAroundCells(matrix, cell.x, cell.y)
  cells.forEach(c => c.minesAround += 1)
}

function getCell(matrix, x, y) {
  if (!matrix[y] || !matrix[y][x]) return false
  return matrix[y][x]
}

function getAroundCells(matrix, x, y) {
  const cells = []
  for (let dx = -1; dx <= 1; dx++) {
    for (let dy = -1; dy <= 1; dy++) {
      if (dx === 0 && dy === 0) {
        continue
      }
      const cell = getCell(matrix, x + dx, y + dy)
      cell && cells.push(cell)
    }
  }
  return cells
}

function getCellById(matrix, id) {
  for (let y = 0; y < matrix.length; y++) {
    for (let x = 0; x < matrix[y].length; x++) {
      const cell = matrix[y][x]
      if (cell.id === id) {
        return cell
      }
    }
  }
  return false
}

function matrixToHtml(matrix) {
  const gameElement = document.createElement('div')
  gameElement.classList.add('sapper')

  for (let y = 0; y < matrix.length; y++) {
    const rowElement = document.createElement('div')
    rowElement.classList.add('row')

    for (let x = 0; x < matrix[y].length; x++) {
      const cell = matrix[y][x]
      const imgElement = document.createElement('img')
      imgElement.style.border = '1px solid black'
      imgElement.style.boxSizing = 'border-box'
      imgElement.draggable = false
      imgElement.oncontextmenu = () => false
      imgElement.setAttribute('data-cell-id', cell.id)
      rowElement.append(imgElement)
      if (cell.flag) {
        imgElement.src = 'assets/045-ambulance.svg'
        imgElement.style.backgroundColor = '#545252'
        continue
      }
      if (cell.poten) {
        imgElement.style.backgroundColor = '#797777'
        continue
      }
      if (!cell.show) {
        imgElement.style.backgroundColor = '#545252'
        continue
      }
      if (cell.mine) {
        imgElement.src = 'assets/007-virus.svg'
        continue
      }
      if (cell.minesAround > 0) {
        imgElement.src = `assets/number${cell.minesAround}.png`
        continue
      }

    }
    gameElement.appendChild(rowElement)
  }

  return gameElement
}

function forEachCell(matrix, handler) {
  for (let y = 0; y < matrix.length; y++) {
    for (let x = 0; x < matrix[y].length; x++) {
      handler(matrix[y][x])
    }
  }
}

function isWin(matrix) {
  const flags = []
  const mines = []
  forEachCell(matrix, c => {
    c.flag && flags.push(c)
    c.mine && mines.push(c)
  })
  if (flags.length !== mines.length) return false
  for (const cell of mines) {
    if (!cell.flag) return false
  }
  forEachCell(matrix, c => {
    if (!c.mine && !c.show) return false
  })
  return true
}

function isLosing(matrix) {
  let lose = false
  forEachCell(matrix, c => {
    if (c.mine && c.show) {
      lose = true
    }
  })
  return lose
}