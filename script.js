let matrix = null
let running = null
document
  .querySelector('button')
  .addEventListener('click', () => init(10, 10, 10))

update()

init(10, 10, 10)

function init(cols, rows, mines) {
  matrix = getMatrix(cols, rows)
  running = true
  for (let i = 0; i < mines; i++) {
    setRandomMine(matrix)
  }
  update()
}


function update() {
  if (!running) return
  const gameElement = matrixToHtml(matrix)
  const appElement = document.querySelector('#app')
  appElement.innerHTML = ''
  appElement.append(gameElement)

  appElement
    .querySelectorAll('img')
    .forEach(imgElement => {
      imgElement.addEventListener('mousedown', mousedownHandler)
      imgElement.addEventListener('mouseup', mouseupHandler)
      imgElement.addEventListener('mouseleave', mouseleaveHandler)
    })

  if (isLosing(matrix) === true) {
    alert('Вы проиграли')
    running = false
  }
  if (isWin(matrix)) {
    alert('Вы выиграли! Поздравляю!')
    running = false
  }
}

function getInfo(event) {
  const el = event.target
  const cellId = parseInt(el.getAttribute('data-cell-id'))

  return {
    left: event.which === 1,
    right: event.which === 3,
    cell: getCellById(matrix, cellId)
  }
}

function mousedownHandler(event) {
  const {left, right, cell} = getInfo(event)
  if (left) cell.left = true;
  if (right) cell.right = true;
  if (cell.left && cell.right) bothHandler(cell)
  update()
}

function mouseupHandler(event) {
  const {left, right, cell} = getInfo(event)
  const both = cell.left && cell.right && (left || right)
  const leftMouse = !both && cell.left && left
  const rightMouse = !both && cell.right && right

  if (both) {
    forEachCell(matrix, (cell => cell.poten = false))
  }

  if (left) cell.left = false;
  if (right) cell.right = false;

  leftMouse && leftHandler(cell)
  rightMouse && rightHandler(cell)

  update()
}

function mouseleaveHandler(event) {
  const info = getInfo(event)
  info.cell.left = false;
  info.cell.right = false;
  update()
}

function leftHandler(cell) {
  if (cell.show || cell.flag) return
  cell.show = true

  showSpread(matrix, cell.x, cell.y)
}

function rightHandler(cell) {
  if (!cell.show) cell.flag = !cell.flag
}

function bothHandler(cell) {
  if (!cell.show || !cell.minesAround) return
  const cells = getAroundCells(matrix, cell.x, cell.y)
  const flags = cells.filter(c => c.flag).length

  if (flags === cell.minesAround) {
    cells
      .filter(c => !c.flag && !c.show)
      .forEach(c => {
        c.show = true
        showSpread(matrix, c.x, c.y)
      })
  } else {
    cells
      .filter(c => !c.flag && !c.show)
      .forEach(c => c.poten = true)
  }
}


function showSpread(matrix, x, y) {
  const cell = getCell(matrix, x, y)
  if (cell.flag || cell.minesAround || cell.mine) return
  forEachCell(matrix, c => c._marked = false)
  cell._marked = true
  let flag = true
  while (flag) {
    flag = false
    for (let y = 0; y < matrix.length; y++) {
      for (let x = 0; x < matrix[y].length; x++) {
        const cell = matrix[y][x]
        if (!cell._marked || cell.minesAround) continue
        const cells = getAroundCells(matrix, x, y)
        for (const c of cells) {
          if (c._marked) continue
          if (!c.mine && !c.flag) {
            c._marked = true
            flag = true
          }
        }
      }
    }
  }
  forEachCell(matrix, c => {
      if (c._marked) c.show = true
      delete c._marked
    }
  )
}